const fs = require ('fs');
const csvToObj = require('csv-to-js-parser').csvToObj;
const path = require('path');
const data = fs.readFileSync(path.resolve(__dirname,'./deliveries.csv')).toString();
let deliveriesObj = csvToObj(data);

module.exports = deliveriesObj;