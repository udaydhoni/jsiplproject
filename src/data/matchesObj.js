const fs = require ('fs');
const csvToObj = require('csv-to-js-parser').csvToObj;
const path = require('path');
const data = fs.readFileSync(path.resolve(__dirname,'./matches.csv')).toString();
let matchesObj = csvToObj(data);

module.exports = matchesObj;