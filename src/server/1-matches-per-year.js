const fs = require('fs');

const path = require('path');

let matchesObj = require('../data/matchesObj');


function matchesPerYear (dataArray) {
    let req = {};
    for (let index = 0; index < dataArray.length; index++) {
        if (req[dataArray[index]['season']]){
            req[dataArray[index]['season']] +=1;
        }else {
            req[dataArray[index]['season']] =1;
        }
    }
    return req;
}

let result = matchesPerYear(matchesObj);

const resultInJson = JSON.stringify(result,null,2)

fs.writeFileSync(path.resolve(__dirname,'../public/output/matchesPerYear.json'), resultInJson);