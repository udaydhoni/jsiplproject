const fs = require('fs');
const path = require('path');

let matchesObj = require('../data/matchesObj.js');

function matchesWonPerTeam (dataArray) {
    let result = {};
    for (let index = 0 ; index < dataArray.length ; index++) {
        let team = dataArray[index]['winner'];
        let season = dataArray[index]['season'];
        if (result[team]) {
            if (result[team][season]) {
                result[team][season] +=1;
            }else {
                result[team][season] =1
            }
        } else {
            result[team] = {};
            result[team][season] =1;
        }
    }
    return result;
}

let result = matchesWonPerTeam(matchesObj);

const resultInJson = JSON.stringify(result,null,2);

fs.writeFileSync(path.resolve(__dirname,'../public/output/matchesWonPerTeamPerYear.json'), resultInJson);