const fs = require('fs');
const path = require('path');
const deliveriesObj = require('../data/deliveriesObj.js');
const matchesObj = require('../data/matchesObj.js');

function extraRunsPerTeam (deliveryDataArray,matchesDataArray,year) {
    let matchId = [];
    matchesDataArray.map((elem) => {
        if (elem['season'] == year) {
            matchId.push(elem['id']);
        }
    })
    let start = Math.min(...matchId);
    let end = Math.max(...matchId);
    console.log(start,end);
    let result = {};
    deliveryDataArray.map((elem) => {
        if (elem['match_id']>= start && elem['match_id']<=end) {
            if (result[elem['bowling_team']]) {
                result[elem['bowling_team']] += Number(elem['extra_runs']);
            }else {
                result[elem['bowling_team']] = Number(elem['extra_runs']);
            }
        }
    })
    return result;
    
}

let result = extraRunsPerTeam(deliveriesObj,matchesObj,2016);
let resultInJson = JSON.stringify(result,null,2);

fs.writeFileSync(path.resolve(__dirname,'../public/output/extraRunsPerTeam.json'),resultInJson);