const fs = require('fs');
const path = require('path');
const deliveriesObj = require('../data/deliveriesObj.js');
const matchesObj = require('../data/matchesObj.js');

function TenEconBowlers (dDataArray,mDataArray,year) {
    let matchID = [];
    mDataArray.map((elem) => {
        if (elem['season'] == year) {
            matchID.push(elem['id']);
        }
    })
    let start = Math.min(...matchID);
    let end = Math.max(...matchID);
    let bowlerData  = dDataArray.reduce((accum,elem) => {
        if (elem['match_id']>=start && elem['match_id']<=end) {
            if (accum[elem['bowler']]) {
                accum[elem['bowler']]['runs'] += Number(elem['total_runs']);
                accum[elem['bowler']]['balls'] +=1;
            }else {
                accum[elem['bowler']] = {};
                accum[elem['bowler']]['runs'] = Number(elem['total_runs']);
                accum[elem['bowler']]['balls'] =1
            }
        }
        return accum;
    },{})
    let bowlers = Object.keys(bowlerData);
    let array = [];
    bowlers.map((elem) => {
        array.push([elem,bowlerData[elem]['runs']/bowlerData[elem]['balls']])
    })
    array.sort((a,b)=> (a[1]-b[1]));
    let result = [];
    for (let index = 0 ; index < 10 ; index++) {
        result.push(array[index][0]);
    }
    return result;
}

let resultInJson =JSON.stringify(TenEconBowlers(deliveriesObj,matchesObj,2015),null,2);

fs.writeFileSync(path.resolve(__dirname,'../public/output/economicalBowlers.json'),resultInJson);