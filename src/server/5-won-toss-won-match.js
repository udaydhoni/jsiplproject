const fs = require('fs');
const path = require('path');

const matchesObj = require('../data/matchesObj.js');

function wonTossWonMatch(dataArray) {
    let count ={};
    for (let index =0 ; index < dataArray.length ; index++) {
        if (dataArray[index]['toss_winner'] === dataArray[index]['winner']) {
            if (count[dataArray[index]['toss_winner']]) {
                count[dataArray[index]['toss_winner']] +=1
            }else {
                count[dataArray[index]['toss_winner']] =1;
            }
        }
    }
    return count;
}

let resultInJson = JSON.stringify(wonTossWonMatch(matchesObj),null,2);

fs.writeFileSync(path.resolve(__dirname,'../public/output/WonTossWonMatch.json'),resultInJson);