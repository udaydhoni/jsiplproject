const fs = require('fs');
const path = require('path');
const matchesObj = require('../data/matchesObj.js');

function highestPlayerOfMatchInASeason (mDataArray) {
    let season = {};
    season = mDataArray.reduce((accu,elem) => {
        if (accu[elem['season']]) {
            if (accu[elem['season']][elem['player_of_match']]) {
                accu[elem['season']][elem['player_of_match']] += 1;
            }else {
                accu[elem['season']][elem['player_of_match']] = 1 ;
            }
        }else {
            accu[elem['season']] = {};
            accu[elem['season']][elem['player_of_match']] = 1;
        }
        return accu;
    },{});
    for (let years in season) {
        season[years] = Object.entries(season[years]);
        season[years] = season[years].sort((a,b) => b[1]-a[1]);
        season[years] = season[years][0][0];
    }
    return season;  
}

let resultInJson = JSON.stringify(highestPlayerOfMatchInASeason(matchesObj),null,2);

fs.writeFileSync(path.resolve(__dirname,'../public/output/highestPoms.json'),resultInJson);