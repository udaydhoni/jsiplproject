const fs = require ('fs');
const path = require('path');
const deliveriesObj = require('../data/deliveriesObj.js');
const matchesObj = require('../data/matchesObj.js');

function strikeRateForEachSeason(batsman,dData,mData) {
    let batsmanData = {};
    let matchIdForSeason = {};
    for (let index = 0 ; index < mData.length ; index++) {
        if (matchIdForSeason[mData[index]['season']]) {
            matchIdForSeason[mData[index]['season']].push(mData[index]['id']);
        } else {
            matchIdForSeason[mData[index]['season']] = [mData[index]['id']];
        }
    }
    console.log(matchIdForSeason);
    batsmanData=dData.reduce((accum,elem) => {
        let seasonForMatchId = '';
        for (let years in matchIdForSeason) {
            if (matchIdForSeason[years].indexOf(elem['match_id']) != -1) {
                seasonForMatchId = years
            }
        }
        if (accum[elem['batsman']]) {
            if (accum[elem['batsman']][seasonForMatchId]) {
                accum[elem['batsman']][seasonForMatchId]['runs'] += Number(elem['batsman_runs']);
                accum[elem['batsman']][seasonForMatchId]['balls'] +=1;

            }else {
                accum[elem['batsman']][seasonForMatchId] = {};
                accum[elem['batsman']][seasonForMatchId]['runs'] = Number(elem['batsman_runs']);
                accum[elem['batsman']][seasonForMatchId]['balls'] =1;
            }
        }else {
            accum[elem['batsman']] = {};
            accum[elem['batsman']][seasonForMatchId] = {}  ;
            accum[elem['batsman']][seasonForMatchId]['runs'] = Number(elem['batsman_runs']);
            accum[elem['batsman']][seasonForMatchId]['balls'] =1;
        }
        return accum;
    },{})
    for (let players in batsmanData) {
        if (players === batsman) {
            for (let years in batsmanData[players]) {
                batsmanData[players][years] = (batsmanData[players][years]['runs'] * 100) /batsmanData[players][years]['balls'];
            }
            return batsmanData[players];
        }
    }
}

let resultInJson = JSON.stringify(strikeRateForEachSeason('MS Dhoni',deliveriesObj,matchesObj),null,2);

fs.writeFileSync(path.resolve(__dirname,'../public/output/strikeRate.json'),resultInJson);