const fs = require('fs');
const path = require('path');
const deliveriesObj = require('../data/deliveriesObj.js');

function highestNumber (dataArray) {
    dataArray = dataArray.filter((elem)=> elem['player_dismissed']!=null)
    let combo = [];
    dataArray.map((elem)=> combo.push(elem['player_dismissed'].concat(' by ',elem['bowler'])))
    let frequency = combo.reduce((accum,elem) => {
        if (accum[elem]) {
            accum[elem] +=1;
        } else {
            accum[elem] = 1;
        }
        return accum
    },{})
    let keys = Object.keys(frequency);
    console.log(keys)
    let maxi = frequency[keys[0]];
    let resultant = '';
    for (let out in frequency) {
        if (frequency[out] >= maxi) {
            resultant = ''
            maxi = frequency[out];
            resultant += out;
        }
    }
    return resultant.concat(' ',maxi);
    
}

let resultInJson = JSON.stringify(highestNumber(deliveriesObj),null,2);

fs.writeFileSync(path.resolve(__dirname,'../public/output/dismissalCombo.json'),resultInJson);