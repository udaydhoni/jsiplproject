const fs = require('fs');
const path = require('path');
const deliveriesObj =require('../data/deliveriesObj.js');
function bestBowler (x) {
     x = x.filter((elem) => elem['is_super_over'] == 1);
    let bowlerFigures = x.reduce((accum,elem) => {
        if (accum[elem['bowler']]) {
            accum[elem['bowler']]['runs'] += parseInt(elem['total_runs']);
            accum[elem['bowler']]['balls'] +=1;
        }else {
            accum[elem['bowler']] ={};
            accum[elem['bowler']]['runs']= parseInt(elem['total_runs']);
            accum[elem['bowler']]['balls'] =1;
        }
        return accum;
    }, {})
    let z = [];
    for (let players in bowlerFigures) {
        let arr = [players, bowlerFigures[players]['runs']/bowlerFigures[players]['balls']];
        z.push(arr);
    }
    z.sort((a,b)=>a[1]-b[1]);
    return(z[0][0]);
}

let resultInJson = JSON.stringify(bestBowler(deliveriesObj),null,2)
fs.writeFileSync(path.resolve(__dirname,'../public/output/bestEconomy.json'),resultInJson);